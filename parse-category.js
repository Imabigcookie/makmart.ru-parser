const parseProductInfo = require('./parse-product-info')

parseCategory = async (page, link) => {
  await page.goto(link)

  const productLinks = await page.$$eval('.catalog_block.items.block_list > .item_block .catalog_item a.thumb.shine', links => links.map(link => link.href))

  for (const productLink of productLinks) {
    await parseProductInfo(page, productLink)
  }

  console.log(`Page ${link} done!`)
  await page.goto(link)
  try {
    const categoryNextPageLink = await page.$eval('.module-pagination .flex-nav-next > a', link => link.href)

    categoryNextPageLink && await parseCategory(page, categoryNextPageLink)
  } catch (err) {
  }
}

module.exports = parseCategory
