const puppeteer = require('puppeteer');
const catalogParser = require('./parse-category');
const clearProject = require('./clear-project');

(async () => {
  await clearProject()

  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  const catalogLinks = ['https://makmart.ru/info/brands/inoxa/']

  for (const catalogLink of catalogLinks) {
    await catalogParser(page, catalogLink)
    console.log('category parsed')
  }


  await browser.close()
  console.log('Done!')
})();
