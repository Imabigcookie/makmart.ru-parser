module.exports = async (page) => {
  try {
    const relatedCodsArray = await page.$$eval('#related > .productList > .product > .tabloid > .rating', cods => cods.map(cod => cod.innerText))
    const formattedCodsString = relatedCodsArray.map(code => code.split(' ')[1]).join(' // ')

    if (!relatedCodsArray.length) return null

    return formattedCodsString
  } catch (err) {
    return ''
  }
}
