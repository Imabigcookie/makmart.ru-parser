module.exports = async (page) => {
  const cellNames = await page.$$eval('.wd_propsorter table .cell_name', names => names.map(name => name.innerText))
  const cellValues = await page.$$eval('.wd_propsorter table .cell_value', values => values.map(value => value.innerText))

  const resultChars = cellNames.reduce((acc, cur, index) => {
    if (!cur) return

    acc[cur] = cellValues[index]

    return acc
  }, {})

  return resultChars
}
