module.exports = async (page) => {
  const allBreadcrumbs = await page.$$eval('#navigation .bx-breadcrumb-item', items => items.map(item => item.innerText))

  allBreadcrumbs.splice(0, 2)

  return allBreadcrumbs
}
