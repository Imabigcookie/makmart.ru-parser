const fs = require('fs');
const axios = require('axios');
const path = require('path');

downloadFile = async (link, productCode) => {
  const url = link
  const fileName = link.split('/').slice(-1).pop()
  const filePath = path.resolve(`documents/${productCode}/files`, fileName)
  const writer = fs.createWriteStream(filePath)

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  })

  response.data.pipe(writer)

  return new Promise((res, rej) => {
    writer.on('finish', res)
    writer.on('error', rej)
  })
}

module.exports = async (page, productCode) => {
  let downloadLink
  const newProductCode = productCode.replace(/\//g, '+')
  const dirPath = path.resolve(`documents/${newProductCode}/files`)

  try {
    downloadLink = await page.$eval(
      '.files_block .description a',
      link => link.href
    )
  } catch (err) {
    return
  }

  fs.mkdirSync(dirPath, { recursive: true })

  downloadLink && await downloadFile(downloadLink, newProductCode)
}
