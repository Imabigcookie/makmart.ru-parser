module.exports = async (page) => {
  try {
    const description = await page.$eval('.block-detailtext', div => div.innerHTML)

    return description
  } catch (err) {
    return null
  }
}
